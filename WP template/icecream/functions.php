<?php
/**
 * Icecream functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Icecream
 */

if ( ! function_exists( 'icecream_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function icecream_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Icecream, use a find and replace
	 * to change 'icecream' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'icecream', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'icecream' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'icecream_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'icecream_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function icecream_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'icecream_content_width', 640 );
}
add_action( 'after_setup_theme', 'icecream_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function icecream_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Text & search', 'icecream' ),
		'id'            => 'textandsearch',
		'description'   => esc_html__( 'Text and search fields.', 'icecream' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Recent', 'icecream' ),
		'id'            => 'recent',
		'description'   => esc_html__( 'Recent posts.', 'icecream' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'icecream_widgets_init' );

/**
 * Register custom menu
 */
function my_register_nav_menus() {
	register_nav_menu( 'social', __( 'Social Links Menu', 'icecream' ) );
}
add_action( 'init', 'my_register_nav_menus' );

/**
 * Enqueue scripts and styles.
 */
function icecream_scripts() {
	wp_enqueue_style( 'icecream-style', get_stylesheet_uri() );
	
	wp_enqueue_style( 'icecream-custom-style-reset', get_template_directory_uri() . '/resources/custom-css/reset.css' );
	
	wp_enqueue_style( 'icecream-custom-font', get_template_directory_uri() . '/resources/custom-fonts/genericons/genericons.css' );
	
	wp_enqueue_style( 'icecream-custom-style', get_template_directory_uri() . '/resources/custom-css/style.less' );
	
	wp_enqueue_script( 'icecream-jquery', get_template_directory_uri() . '/resources/custom-js/jquery-2.2.2.min.js' );

	wp_enqueue_script( 'icecream-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'icecream-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'icecream_scripts' );

/**
 * Turn off sticky posts.
 */
function turn_off_sticky( $query ) {
	$query->set( 'ignore_sticky_posts', true );
}
add_action( 'pre_get_posts', 'turn_off_sticky' );

/* Shortcode contact */
function contact_shortcode($content = null) {
	ob_start();
	get_template_part('contact');
	return ob_get_clean();
	}
add_shortcode('contact', 'contact_shortcode');

/* Set  custom navigation*/
function get_the_custom_post_navigation( $args = array() ) {
	$args = wp_parse_args( $args, array(
		'prev_text'          => '%title',
		'next_text'          => '%title',
		'in_same_term'       => false,
		'excluded_terms'     => '',
		'taxonomy'           => 'category',
		'screen_reader_text' => __( 'Post navigation' ),
	) );

	$navigation = '';

	$previous = get_previous_post_link(
		'<div class="nav-previous nav-link">%link</div>',
		$args['prev_text'],
		$args['in_same_term'],
		$args['excluded_terms'],
		$args['taxonomy']
	);

	$next = get_next_post_link(
		'<div class="nav-next nav-link">%link</div>',
		$args['next_text'],
		$args['in_same_term'],
		$args['excluded_terms'],
		$args['taxonomy']
	);

	if ( $previous || $next ) {
		$navigation = _navigation_markup( $previous . $next, 'post-navigation', $args['screen_reader_text'] );
	}

	return $navigation;
}

function get_the_custom_posts_navigation( $args = array() ) {
	$navigation = '';

	if ( $GLOBALS['wp_query']->max_num_pages > 1 ) {
		$args = wp_parse_args( $args, array(
			'prev_text'          => '&larr; Older posts',
			'next_text'          => 'Newer posts &rarr;',
			'screen_reader_text' => __( 'Posts navigation' ),
		) );

		$next_link = get_previous_posts_link( $args['next_text'] );
		$prev_link = get_next_posts_link( $args['prev_text'] );

		if ( $prev_link ) {
			$navigation .= '<div class="nav-previous nav-link">' . $prev_link . '</div>';
		}

		if ( $next_link ) {
			$navigation .= '<div class="nav-next nav-link">' . $next_link . '</div>';
		}

		$navigation = _navigation_markup( $navigation, 'paging-navigation', $args['screen_reader_text'] );
	}

	return $navigation;
}

add_filter('next_posts_link_attributes', 'sdac_next_posts_link_attributes');
function sdac_next_posts_link_attributes(){
        return 'class="meta-nav"';
}
 
add_filter('previous_posts_link_attributes', 'sdac_previous_posts_link_attributes');
function sdac_previous_posts_link_attributes(){
        return 'class="meta-nav"';
}

/* Adding custom navigation*/
function the_custom_post_navigation( $args = array() ) {
	echo get_the_custom_post_navigation( $args );
}

function the_custom_posts_navigation( $args = array() ) {
	echo get_the_custom_posts_navigation( $args );
}

/* Set custom searchform */
function my_search_form( $form ) {
	$form = '<form role="search" method="get" class="search-form" action="' . home_url( '/' ) . '" >
		<label>
			<span class="screen-reader-text">Search for:</span>
			<input type="search" class="search-field" placeholder="Enter keyword and hit enter" value="' . get_search_query() . '" name="s" id="Serach for:" />
		</label>
		<input type="submit" class="search-submit" value="Search" />
	</form>';

	return $form;
}

add_filter( 'get_search_form', 'my_search_form' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load TGM.
 */
require get_template_directory(__FILE__) . '/resources/tgm/icecream.php';

/**
 * Custom walker for soical menu.
 */
class Socal_menu extends Walker_Nav_Menu {
	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output
	 * @param object $item Объект элемента меню, подробнее ниже.
	 * @param int $depth Уровень вложенности элемента меню.
	 * @param object $args Параметры функции wp_nav_menu
	 */
	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
		global $wp_query;           
		/*
		 * Некоторые из параметров объекта $item
		 * ID - ID самого элемента меню, а не объекта на который он ссылается
		 * menu_item_parent - ID родительского элемента меню
		 * classes - массив классов элемента меню
		 * post_date - дата добавления
		 * post_modified - дата последнего изменения
		 * post_author - ID пользователя, добавившего этот элемент меню
		 * title - заголовок элемента меню
		 * url - ссылка
		 * attr_title - HTML-атрибут title ссылки
		 * xfn - атрибут rel
		 * target - атрибут target
		 * current - равен 1, если является текущим элементов
		 * current_item_ancestor - равен 1, если текущим является вложенный элемент
		 * current_item_parent - равен 1, если текущим является вложенный элемент
		 * menu_order - порядок в меню
		 * object_id - ID объекта меню
		 * type - тип объекта меню (таксономия, пост, произвольно)
		 * object - какая это таксономия / какой тип поста (page /category / post_tag и т д)
		 * type_label - название данного типа с локализацией (Рубрика, Страница)
		 * post_parent - ID родительского поста / категории
		 * post_title - заголовок, который был у поста, когда он был добавлен в меню
		 * post_name - ярлык, который был у поста при его добавлении в меню
		 */
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
 
		/*
		 * Генерируем строку с CSS-классами элемента меню
		 */
		$class_names = $value = '';
		$classes = empty( $item->classes[0] ) ? array() : (array) $item->classes[0];
		//$classes[] = 'menu-item-' . $item->ID;
 
		// функция join превращает массив в строку
		$class_names_clean = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names_clean ) . '"';
 
		/*
		 * Генерируем ID элемента
		 */
		//$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		//$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
 
		/*
		 * Генерируем элемент меню
		 */
		$output .= $indent . '<li' . $id . $value . $class_names .'>';
 
		// атрибуты элемента, title="", rel="", target="" и href=""
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
 
		// ссылка и околоссылочный текст
		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= '<span class="genericon genericon-' . $class_names_clean . '">';
		$item_output .= $args->link_before /*. apply_filters( 'the_title', $item->title, $item->ID )*/ . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;
 
 		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}