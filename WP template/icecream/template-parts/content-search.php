<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Icecream
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class("post-grid"); ?>style="background-image: url(<?php $thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'image' ); echo $thumbnail_attributes[0]; ?>)";>
	<div class="post-grid-content">
		<?php echo '<a href="' . esc_url( get_permalink() ) . '" class="item-link" rel="bookmark">', '</a>' ?>
		<div class="text-container">
			<header class="entry-header">
				<?php if(is_sticky()) echo '<span class="sticky-post-label">Featured</span>'; ?>
				<?php
					if ( is_single() ) {
						the_title( '<h1 class="entry-title">', '</h1>' );
					} else {
						the_title( '<h2 class="entry-title">', '</h2>' );
					}
				?>
			</header><!-- .entry-header -->

			<footer class="entry-footer">
				<?php icecream_posted_on(); ?>
				<?php icecream_entry_footer(); ?>
			</footer><!-- .entry-footer -->
		</div>
		<div class="overlay"></div>
	</div>
</article><!-- #post-## -->
