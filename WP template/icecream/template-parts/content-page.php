<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Icecream
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<section class="page-cover" style="background-image: url(<?php $thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'image' ); echo $thumbnail_attributes[0]; ?>)";>
		<div class="overlay"></div>
		<div class="page-cover-inside inner-block">
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->
		</div>
	</section>

	<div class="entry-content">
		<?php
			the_content();
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
