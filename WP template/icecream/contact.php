<?php
if (isset($_POST['submitted'])) {
    if (trim($_POST['contactName']) === '') {
        $nameError = __('Please enter your name', 'icecream');
        $hasError = true;
    } else {
        $name = trim($_POST['contactName']);
    }

    if (trim($_POST['email']) === '') {
        $emailError = __('Please enter your email address', 'icecream');
        $hasError = true;
    } else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email']))) {
        $emailError = __('You entered an invalid email address', 'icecream');
        $hasError = true;
    } else {
        $email = trim($_POST['email']);
    }

    if (trim($_POST['messege']) === '') {
        $commentError = __('Please enter a message', 'icecream');
        $hasError = true;
    } else {
        $messege = stripslashes(trim($_POST['messege']));
    }

    if (!isset($hasError)) {
        $emailTo = get_option('admin_email');

        if (trim($_POST['subject']) != '') {
            $subject = trim($_POST['subject']);
        } else {
            $subject = '';
        }

        $body = __('From', 'icecream') . ": $name <$email>\n\n" . __('Messege', 'icecream') . ":\n$messege\n\n--\n" . __('This mail is sent via contact form on ', 'icecream') . get_bloginfo('name') . "\n" . get_bloginfo('url');
        //$headers[] = "From: $name";
        //$headers[] = "Reply-To: $email";
        //$headers[] = "To: $emailTo";
        wp_mail($emailTo, $subject, $body,$headers);
        $emailSent = true;
        unset($_POST['contactName'], $_POST['email'], $_POST['subject'], $_POST['messege']);
    }
}
?>

<?php if (isset($emailSent) && $emailSent == true) { ?>
    <div class="good">
        <p><?php _e('Thanks, your email was sent successfully', 'icecream'); ?></p>
    </div>
<?php } ?>

<div id="contact-form-<?php the_ID(); ?>">
	<form action="<?php the_permalink(); ?>" method="post" class="contact-form commentsblock">
		<div>
			<label><?php _e('Name', 'icecream'); ?><span>(required)</span>
				<input type="text" size="40" name="contactName" id="contactName" value="<?php if (isset($_POST['contactName'])) echo $_POST['contactName']; ?>" class="contact-form required" />
				<?php if ($nameError != '') { ?>
					<span class="error"><?php echo $nameError; ?></span>
				<?php } ?>
			</label>
		</div>
		<div>
			<label><?php _e('E-mail', 'icecream'); ?><span>(required)</span>
				<input type="text" size="40" name="email" id="email" value="<?php if (isset($_POST['email'])) echo $_POST['email']; ?>" class="contact-form required email" />
				<?php if ($emailError != '') { ?> 
					<span class="error"><?php echo $emailError; ?></span>
				<?php } ?>
			</label>
		</div>
		<div>
			<label><?php _e('Subject', 'icecream'); ?>
				<input type="text" size="40" name="subject" id="subject" value="<?php if (isset($_POST['subject'])) echo $_POST['subject']; ?>" class="contact-form required subject" />
			</label>
		</div>
		<div>
			<label><?php _e('Message', 'icecream'); ?><span>(required)</span>
				<textarea name="messege" rows="10" cols="70" id="messegeText" rows="20" cols="30" class="contact-form required messege"><?php if (isset($_POST['messege'])) {
					if (function_exists('stripslashes')) {
						echo stripslashes($_POST['messege']);
					} else {
						echo $_POST['messege'];
					}
				} ?></textarea>
		<?php if ($commentError != '') { ?>
					<span class="error"><?php echo $commentError; ?></span>
				<?php } ?>
			</label>
		</div>
		<p class="contact-submit">
			<input type="submit" value="<?php _e('Submit »', 'icecream'); ?>"></input>
			<input type="hidden" name="submitted" id="submitted" value="true" />
		</p>
	</form>	
</div>