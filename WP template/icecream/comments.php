<?php
/**
 * The template for displaying comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Icecream
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) : ?>
		<h2 class="comments-title block-title">
			 <span> <?php
					printf( // WPCS: XSS OK.
						esc_html( _nx( '%1$s Comment', '%1$s Comments', get_comments_number(), 'comments title', 'icecream' ) ),
						number_format_i18n( get_comments_number() )
					);
				?>
			</span>
		</h2>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
			<h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'icecream' ); ?></h2>
			<div class="nav-links">

				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'icecream' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'icecream' ) ); ?></div>

			</div><!-- .nav-links -->
		</nav><!-- #comment-nav-above -->
		<?php endif; // Check for comment navigation. ?>
		
		<?php
		  // стандартный вывод комментариев
		  function custom_icecream_comment($comment, $args, $depth){
			$GLOBALS['comment'] = $comment; ?>
			  <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
				<article id="comment-<?php comment_ID(); ?>" class="comment-body">
					<div class="comment-author-wrap">
						<figure class="comment-avatar">
							<?php echo get_avatar($comment,$size='48' ); ?>
						</figure>
						<footer class="comment-meta">
							<?php printf(__('<cite class="comment-author">%s</cite>'), get_comment_author_link()) ?>

							<?php if ($comment->comment_approved == '0') : ?>
							<em><?php _e('Your comment is awaiting moderation.') ?></em>
							<br />
							<?php endif; ?>

							<span class="comment-metadata">
								<a class="time-link" href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?></a>
								<span class="divider">/</span>
								<span class="reply">
									<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'reply_text' => 'Reply', 'login_text' => 'Log in to Reply', 'max_depth' => $args['max_depth']))) ?>
								</span>
								<?php edit_comment_link('(Edit)','  ','') ?>
							</span>
						</footer>
					</div>
					<div class="comment-content">
						<?php comment_text() ?>
					</div>
				</article>
		  <?php }
		?>

		<ul class="comment-list">
			<?php
				wp_list_comments( array(
					'style'      => 'ul',
					'short_ping' => true,
					'callback' => 'custom_icecream_comment',
				) );
			?>
		</ul><!-- .comment-list -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
			<h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'icecream' ); ?></h2>
			<div class="nav-links">

				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'icecream' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'icecream' ) ); ?></div>

			</div><!-- .nav-links -->
		</nav><!-- #comment-nav-below -->
		<?php
		endif; // Check for comment navigation.

	endif; // Check for have_comments().


	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

		<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'icecream' ); ?></p>
	<?php
	endif;
	
	$commenter = wp_get_current_commenter();
	$comment_args = array(
		'fields'               => array(
									'author' => '<p class="comment-form-author">' . '<label for="author">Name' . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
												'<input id="author" name="author" type="text" value="" size="30"' . $aria_req . $html_req . ' /></p>',
									'email'  => '<p class="comment-form-email"><label for="email">Email' . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
												'<input id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="" size="30" aria-describedby="email-notes"' . $aria_req . $html_req  . ' /></p>',
									'url'    => '<p class="comment-form-url"><label for="url">Website</label> ' .
												'<input id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="" size="30" /></p>',
								),
		'comment_field'        => '<p class="comment-form-comment"><label for="comment">Comment</label> <textarea id="comment" name="comment" cols="45" rows="8"  aria-required="true" required="required"></textarea></p>',
		'must_log_in'          => '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
		'logged_in_as'         => '',
		'comment_notes_before' => '<p class="comment-notes"><span id="email-notes">' . 'Your email address will not be published. Required fields are marked *' . '</span>'. ( $req ? $required_text : '' ) . '</p>',
		'comment_notes_after'  => '',
		'id_form'              => 'commentform',
		'id_submit'            => 'submit',
		'class_form'           => 'comment-form',
		'class_submit'         => 'submit',
		'name_submit'          => 'submit',
		'title_reply'          => 'Submit a comment',
		'title_reply_to'       => 'Reply',
		'title_reply_before'   => '<h3 id="reply-title" class="comment-reply-title"><span>',
		'title_reply_after'    => '</span></h3>',
		'cancel_reply_before'  => ' <small>',
		'cancel_reply_after'   => '</small>',
		'cancel_reply_link'    => 'Cancel reply',
		'label_submit'         => 'Submit Comment',
		'submit_button'        => '<input name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s" />',
		'submit_field'         => '<p class="form-submit">%1$s %2$s</p>',
		'format'               => 'xhtml',
		);
		
	comment_form( $comment_args );
	?>

</div><!-- #comments -->
