<?php
/**
 * Custom social menu.
 *
 * @package Icecream
 */
 
if ( has_nav_menu( 'social' ) ) :

	// Social links navigation menu.
	wp_nav_menu( array(
		'theme_location' => 'social',
		'container' => false,
		'menu_class' => 'footer-social-links',
		'menu_id' => '',
		'depth' => 1,
		'walker' => new Socal_menu,
	) );

	endif;

?>
