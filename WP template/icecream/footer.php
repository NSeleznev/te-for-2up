<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Icecream
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-inner-wrapper">
			<div class="widgets-blocks">
				<?php get_sidebar('recent'); ?>
				<?php get_sidebar('textandsearch'); ?>
			</div>
			<div class="footer-text-container">
				<div class="footer-text">
					<p>No Developers were harmed in the making of this theme.</p>
				</div><!-- .site-info -->
				<?php get_template_part( 'social-menu', 'social' ); ?>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
