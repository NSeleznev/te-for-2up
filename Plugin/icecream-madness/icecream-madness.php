<?php
/*
Plugin Name: Icecream Madness
Plugin URI: https://vk.com/n.seleznev
Description: Just ice cream calculater for 2UP
Version: 1.0
Author: N.N.Seleznev
Author URI: https://vk.com/n.seleznev
*/

/*  
Copyright 2016  Nikolay Seleznev  (email: nnseleznev@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

global $im_db_version;
$im_db_version = '1.0';

/* *УСТАОВКА, ДЕАКТИВАЦИЯ, УДАЛЕНИЕ ПЛАГИНА* */

// Создаем опцию и таблицу при установке плагина.
function im_db_activation() {
	global $wpdb;
	global $im_db_version;

	$icecream_madness = $wpdb->prefix . 'icecream_madness';
	$icecream_madness_shoplist = $wpdb->prefix . 'icecream_madness_shoplist';
	$charset_collate = $wpdb->get_charset_collate();

	$sql1 = "CREATE TABLE IF NOT EXISTS $icecream_madness (
		id int(10) NOT NULL AUTO_INCREMENT,
		type VARCHAR(250) NOT NULL,
		name VARCHAR(250) NOT NULL,
		price VARCHAR(250) NOT NULL,
		description VARCHAR(250) NOT NULL,
		PRIMARY KEY (id)
	) $charset_collate;";

	$sql2 = "CREATE TABLE IF NOT EXISTS $icecream_madness_shoplist (
		id int(10) NOT NULL AUTO_INCREMENT,
		date VARCHAR(250) NOT NULL,
		name VARCHAR(250) NOT NULL,
		price VARCHAR(250) NOT NULL,
		userID VARCHAR(250) NOT NULL,
		bought VARCHAR(250) NOT NULL DEFAULT'false',
		PRIMARY KEY (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	$wpdb->query($sql1);
	$wpdb->query($sql2);

	add_option( 'im_db_version', $im_db_version );
}

// Удаляем опцию при деактивации плагина.
function im_db_deactivation() {
	delete_option( 'im_db_version' );
}

// Удаляем таблицу при деинсталяции плагина.
function im_db_uninstall()
{
	global $wpdb;
	$icecream_madness = $wpdb->prefix . 'icecream_madness';
	$icecream_madness_shoplist = $wpdb->prefix . 'icecream_madness_shoplist';
	$sql1 = "DROP TABLE $icecream_madness";
	$sql2 = "DROP TABLE $icecream_madness_shoplist";

	$wpdb->query( $sql1 );
	$wpdb->query( $sql2 );
}

/* *АКТИВИРУЕМ КУКИ* */

function set_newuser_cookie() {
    if (!isset($_COOKIE['unregistered_user']) && !is_user_logged_in()) {
        setcookie('unregistered_user', time()+1209600);
    }
}

/* *СОЗДАНИЕ МЕНЮ И ФОРМ В АДМИНИСТРАТИВНОЙ ПАНЕЛИ* */

// Регистрируем пункт меню в панели администратора.
function register_im_admin_menu() {
$page_title = 'Icecream Madness';
$menu_title = 'Icecream<br>Madness';
$capability = 'manage_options';
$menu_slug = 'icecream-madness';
$function = 'im_admin_menu';
$icon_url = 'dashicons-smiley';
$position = 2;

add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
}

// Создание и вывод формы добавления компонента
function im_add_component() {
	global $wpdb;
	$icecream_madness = $wpdb->prefix . 'icecream_madness';
	
	// Сохранение компонента в таблицу
	if ( isset( $_POST['im_add_component_btn'] ) ) {
		
		$im_component_type = $_POST['im_component_type'];
		$im_component_name = $_POST['im_component_name'];
		$im_component_price = $_POST['im_component_price'];
		$im_component_description = $_POST['im_component_description'];
		
		if ( $im_component_type !== '' && $im_component_name !== '' && $im_component_price !== '' && $im_component_description !== '' ) {
			
			$wpdb->insert(
				$icecream_madness,
				array(
				'type' => $im_component_type,
				'name' => $im_component_name,
				'price' => $im_component_price,
				'description' => $im_component_description),
				array('%s', '%s', '%s', '%s')
			);
			
		} else {
			
			echo 'Заполните все поля';
			
		}
	}
	// Форма добавления компонента
	echo
		'
			<form name="im_add_component" method="post" action="'.$_SERVER['PHP_SELF'].'?page=icecream-madness" style="margin-bottom: 80px;">
				<p>Компонент</p>
				<input type="text" name="im_component_type" style="width: 100%; max-width: 600px;">
				<p>Наименование</p>
				<input type="text" name="im_component_name" style="width: 100%; max-width: 600px;">
				<p>Цена</p>
				<input type="text" name="im_component_price" style="width: 100%; max-width: 600px;">
				<p>Описание</p>
				<textarea name="im_component_description" style="width: 100%; max-width: 600px; height: 100px; max-height: 250px;" maxlength="850"></textarea>
				<div>
					<input type="submit" name="im_add_component_btn" value ="Добавить">
				</div>
			</form>
		';
}

// Создание и вывод формы редактирования компонента
function im_edit_component() {
	global $wpdb;
	$icecream_madness = $wpdb->prefix . 'icecream_madness';
	
	// Редактирование компонента в таблице
	if ( isset( $_POST['im_edit_component_btn'] ) ) {
		
		$im_component_id = $_POST['im_component_id'];
		$im_component_type = $_POST['im_component_type'];
		$im_component_name = $_POST['im_component_name'];
		$im_component_price = $_POST['im_component_price'];
		$im_component_description = $_POST['im_component_description'];
		
		if ( $im_component_type !== '' && $im_component_name !== '' && $im_component_price !== '' && $im_component_description !== '' ) {
			
			$wpdb->update(
				$icecream_madness,
				array(
				'type' => $im_component_type,
				'name' => $im_component_name,
				'price' => $im_component_price,
				'description' => $im_component_description),
				array( 'id' => $im_component_id ),
				array('%s', '%s', '%s', '%s'),
				array( '%d' )
			);
			
		} else {
			
			echo 'Заполните все поля';
			
		}
	}
	
	// Удаление компонента из таблицы
	if ( isset( $_POST['im_delete_component_btn'] ) ) {
		
		$im_component_id = $_POST['im_component_id'];
		
		$wpdb->delete( $icecream_madness, array( 'id' => $im_component_id ), array( '%d' ) );
		
	}
	
	// Форма редактирования компонента
	$im_component = $wpdb -> get_results( "SELECT * FROM $icecream_madness" );
	
	if ( $im_component == null ) {
		
		echo 'Компоненты не найдены';
		
	} else {
	
		foreach ( $im_component as $item ) {
			$im_id = $item->id;
			$im_type = $item->type;
			$im_name = $item->name;
			$im_price = $item->price;
			$im_description = $item->description;
			
			echo
				'
					<h3>'.$im_name.'&nbsp;'.$im_type.'</h3>
					<form name="im_add_component" method="post" action="'.$_SERVER['PHP_SELF'].'?page=icecream-madness" style="border-bottom: 2px solid #ddd; padding-bottom: 20px; margin-bottom: 10px; width: 100%; max-width: 600px;">
						<input type="hidden" name="im_component_id" value="'.$im_id.'" style="width: 100%;">
						<p>Компонент</p>
						<input type="text" name="im_component_type" value="'.$im_type.'" style="width: 100%;">
						<p>Наименование</p>
						<input type="text" name="im_component_name" value="'.$im_name.'" style="width: 100%;">
						<p>Цена</p>
						<input type="text" name="im_component_price" value="'.$im_price.'&nbsp;руб." style="width: 100%;">
						<p>Описание</p>
						<textarea name="im_component_description" style="width: 100%; height: 100px; max-height: 250px;" maxlength="850">'.$im_description.'</textarea>
						<div>
							<input type="submit" name="im_edit_component_btn" value="Сохранить">
							<input type="submit" name="im_delete_component_btn" value="Удалить">
						</div>
					</form>
				';
		}
	}
}

// Код страницы плгина
function im_admin_menu() {
	echo '<h1>Icecream Madness</h1>';
	echo '<h2>Добавить компонент</h2>';
	im_add_component();
	echo '<h2>Список компонентов</h2>';
	im_edit_component();
}

/* *ФОРМА КАЛЬКУЛЯТОРА МОРОЖЕННОГО* */

// Вывод формы калькулятора мороженного

function im_shortcode() {
	
	global $wpdb;
	$icecream_madness = $wpdb->prefix . 'icecream_madness';
	$icecream_madness_shoplist = $wpdb->prefix . 'icecream_madness_shoplist';
	$im_component = $wpdb -> get_results( "SELECT * FROM $icecream_madness" );
	$cookie = ($_COOKIE['unregistered_user']);

	if ( isset( $_POST['im_component_add_to_cart_btn'] ) ) {

		$im_component_id = $_POST['im_component_id'];
		$im_component_price = $wpdb->get_row("SELECT * FROM $icecream_madness WHERE id = $im_component_id");
		$im_component_name = $wpdb->get_row("SELECT * FROM $icecream_madness WHERE id = $im_component_id");

		if ( get_current_user_id() == '0' ) {
			$im_component_shoplist_userID = $cookie;
		} else {
			$im_component_shoplist_userID = get_current_user_id();
		}

		$im_component_shoplist_date = current_time('mysql');
		$im_component_shoplist_name = $im_component_name->name;
		$im_component_shoplist_price = $im_component_price->price;

		$wpdb->insert(
		$icecream_madness_shoplist,
		array(
		'date' => $im_component_shoplist_date,
		'name' => $im_component_shoplist_name,
		'price' => $im_component_shoplist_price,
		'userID' => $im_component_shoplist_userID),
		array('%s', '%s', '%s', '%s')
		);

	}

	if ( $_POST['cart'] == '' ) {
		$im_cart = '0';
	}

	if ( isset( $_POST['im_component_add_to_cart_btn'] ) ) {
		$im_shoplist = $wpdb -> get_results( "SELECT * FROM $icecream_madness_shoplist" );

		foreach ( $im_shoplist as $item ) {
			$im_userID = $item->userID;
		}

		$im_component_shoplist_price_all = $wpdb->get_col("SELECT price FROM $icecream_madness_shoplist WHERE userID = $im_userID AND bought = 'false'");
		$im_component_shoplist_price_sum = array_sum($im_component_shoplist_price_all);

		$im_cart = $im_component_shoplist_price_sum;
	}

	if ( isset( $_POST['buy'] ) ) {
		$wpdb->update($icecream_madness_shoplist,array('bought' => 'true'),array('bought' => 'false'),'%s');
	}
	
	ob_start();

		if ( $im_component == null ) {
		
		echo 'Компоненты не найдены';
		
		} else {
		
			foreach ( $im_component as $item ) {
				$im_id = $item->id;
				$im_type = $item->type;
				$im_name = $item->name;
				$im_price = $item->price;
				$im_description = $item->description;
				
				echo
					'
						<form name="im_component_add_to_cart" method="post" action="'.get_the_permalink().'" style="border-bottom: 2px solid #ddd; padding-bottom: 20px; margin-bottom: 10px; width: 100%;">
							<h3 style="display: inline;">'.$im_name.'&nbsp;'.$im_type.'</h3>
							<input type="hidden" name="im_component_id" value="'.$im_id.'" style="width: 100%;">
							<input style="float:right;" type="submit" name="im_component_add_to_cart_btn" value="Добавить в корзину">
							<input style="float:right; padding: 5px; margin: 2px 10px 0px 0px; width: 100px" type="text" name="im_component_price" value="'.$im_price.'&nbsp;руб.">
							<textarea style="margin-top: 20px;" name="im_component_description" maxlength="850">'.$im_description.'</textarea>
						</form>
					';
			}
			echo '<div class="cart"><input form="im_component_add_to_cart" type="text" name="cart" value="'.$im_cart.'&nbsp;руб.">';
			echo '<form method="post" name="im_buy" action="'.get_the_permalink().'"style="display: inline;"><input type="submit" name="buy" value="Купить" style="margin-left: 10px;"></form></div>';
		}

	return ob_get_clean();
}


function azaza_shortcode() {
	global $current_user;
	global $wpdb;
	$cookie = ($_COOKIE['unregistered_user']);
	$im_shoplist_userID = get_current_user_id();
	$icecream_madness_shoplist = $wpdb->prefix . 'icecream_madness_shoplist';

	if ( $im_shoplist_userID == '0' ){

		$im_shoplist = $wpdb -> get_results( "SELECT * FROM $icecream_madness_shoplist WHERE bought = 'true' AND userID = $cookie" );

	} else {

		$im_shoplist = $wpdb -> get_results( "SELECT * FROM $icecream_madness_shoplist WHERE bought = 'true' AND userID = $im_shoplist_userID" );

	}

	if (!is_user_logged_in()) {
		$im_userNAME = 'Unregistered User';
	} else {
		$im_userNAME = $current_user->user_login;
	}

	

	ob_start();

		if ( $im_shoplist == null ) {
		
		echo 'Ничего не куплено';
		
		} else {
		
			foreach ( $im_shoplist as $item ) {
				$im_id = $item->id;
				$im_date = $item->date;
				$im_name = $item->name;
				$im_price = $item->price;
				$im_userID = $item->userID;
				
				echo
					'
					<h3>Куплено пользователем '.$im_userNAME.'(id = '.$im_userID.')</h3>
						<p style="display: inline-block;"><input type="hidden" name="im_bought_id" value="'.$im_id.'"></p>
						<p style="display: inline-block;"><input type="text" name="im_bought_date" value="'.$im_date.'"></p>
						<p style="display: inline-block;"><input type="text" name="im_bought_name" value="'.$im_name.'"></p>
						<p style="display: inline-block;"><input type="text" name="im_bought_price" value="'.$im_price.'"></p>

					';
			}
		}

	return ob_get_clean();

}

add_action( 'init', 'set_newuser_cookie');
add_shortcode('azaza', 'azaza_shortcode');
add_shortcode('icecream-madness', 'im_shortcode');
add_action( 'admin_menu', 'register_im_admin_menu' );
register_activation_hook( __FILE__, 'im_db_activation' );
register_deactivation_hook( __FILE__, 'im_db_deactivation');
register_uninstall_hook( __FILE__, 'im_db_uninstall');
?>